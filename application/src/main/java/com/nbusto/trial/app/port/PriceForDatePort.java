package com.nbusto.trial.app.port;

import com.nbusto.trial.domain.model.Price;
import java.time.LocalDateTime;
import java.util.Optional;

public interface PriceForDatePort {

  Optional<Price> getPriceFor(Integer brandId, Integer productId, LocalDateTime time);
}
