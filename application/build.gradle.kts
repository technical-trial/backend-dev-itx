plugins {
    java
}
dependencies {
    implementation(project(mapOf("path" to ":domain")))
}

tasks.test {
    useJUnitPlatform()
}