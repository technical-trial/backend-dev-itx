package com.nbusto.trial.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.nbusto.trial.infra", "com.nbusto.trial.app"})
public class TrialApplication {

  public static void main(String[] args) {
    SpringApplication.run(TrialApplication.class, args);
  }
}
