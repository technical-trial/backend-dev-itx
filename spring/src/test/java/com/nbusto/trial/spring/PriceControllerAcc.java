package com.nbusto.trial.spring;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.core.AutoConfigureCache;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@ActiveProfiles("acc")
@AutoConfigureWebMvc
@AutoConfigureMockMvc
@AutoConfigureCache
@SpringBootTest
class PriceControllerAcc {

    @Autowired
    private MockMvc mockMvc;

    @ParameterizedTest
    @MethodSource("testArguments")
    void when_retrieve_data_returned_values_is_right(LocalDateTime applicationDate, Price price)
            throws Exception {
        final var productId = 35455;
        final var brandId = 1;

    mockMvc
        .perform(
            get("/v1/prices/%s/%s".formatted(brandId, productId))
                .contentType("application/json")
                .param("application_date", dateAsString(applicationDate)))
        .andDo(System.out::println)
        .andExpect(status().isOk())
        .andExpect(content().contentType("application/json"))
        .andExpect(jsonPath("$.product_id", is(productId)))
        .andExpect(jsonPath("$.brand_id", is(brandId)))
        .andExpect(jsonPath("$.price_id", is(price.id())))
        .andExpect(jsonPath("$.application_dates", is(notNullValue())))
        .andExpect(jsonPath("$.application_dates.start", is(dateAsString(price.startDate()))))
        .andExpect(jsonPath("$.application_dates.end", is(dateAsString(price.endDate()))));
    }

    private record Price(Integer id, LocalDateTime startDate, LocalDateTime endDate) {}

    private static String dateAsString(LocalDateTime dateTime) {
        return dateTime.format(DateTimeFormatter.ISO_DATE_TIME);
    }

    private static Stream<Arguments> testArguments() {
        return Stream.of(
                Arguments.of(
                        LocalDateTime.of(2020, 6, 14, 10, 0),
                        new Price(
                                1, LocalDateTime.of(2020, 6, 14, 0, 0), LocalDateTime.of(2020, 12, 31, 23, 59, 59))),
                Arguments.of(
                        LocalDateTime.of(2020, 6, 14, 16, 0),
                        new Price(
                                2, LocalDateTime.of(2020, 6, 14, 15, 0), LocalDateTime.of(2020, 6, 14, 18, 30, 0))),
                Arguments.of(
                        LocalDateTime.of(2020, 6, 14, 21, 0),
                        new Price(
                                1, LocalDateTime.of(2020, 6, 14, 0, 0), LocalDateTime.of(2020, 12, 31, 23, 59, 59))),
                Arguments.of(
                        LocalDateTime.of(2020, 6, 15, 10, 0),
                        new Price(
                                3, LocalDateTime.of(2020, 6, 15, 0, 0), LocalDateTime.of(2020, 6, 15, 11, 0, 0))),
                Arguments.of(
                        LocalDateTime.of(2020, 6, 16, 21, 0),
                        new Price(
                                4, LocalDateTime.of(2020, 6, 15, 16, 0), LocalDateTime.of(2020, 12, 31, 23, 59, 59))));
    }
}