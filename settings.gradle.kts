rootProject.name = "trial"
include("domain")
include("application")
include("infrastructure")
include("spring")
