package com.nbusto.trial.domain.model.vo;

import java.math.BigDecimal;

public record Price(BigDecimal value) {}
