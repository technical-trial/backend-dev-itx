package com.nbusto.trial.domain.model.vo;

public record Currency(String value) {}
