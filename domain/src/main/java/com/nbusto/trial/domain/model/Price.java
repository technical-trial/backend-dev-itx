package com.nbusto.trial.domain.model;

import com.nbusto.trial.domain.model.vo.BrandId;
import com.nbusto.trial.domain.model.vo.Currency;
import com.nbusto.trial.domain.model.vo.EndDate;
import com.nbusto.trial.domain.model.vo.PriceList;
import com.nbusto.trial.domain.model.vo.Priority;
import com.nbusto.trial.domain.model.vo.ProductId;
import com.nbusto.trial.domain.model.vo.StartDate;

public record Price(
    BrandId brandId,
    StartDate startDate,
    EndDate endDate,
    PriceList priceList,
    ProductId productId,
    Priority priority,
    com.nbusto.trial.domain.model.vo.Price price,
    Currency currency) {
}
