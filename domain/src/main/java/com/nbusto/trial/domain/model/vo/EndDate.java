package com.nbusto.trial.domain.model.vo;

import java.time.LocalDateTime;

public record EndDate(LocalDateTime value) {}
