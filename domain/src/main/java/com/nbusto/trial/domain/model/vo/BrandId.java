package com.nbusto.trial.domain.model.vo;

public record BrandId(Integer value) {}
