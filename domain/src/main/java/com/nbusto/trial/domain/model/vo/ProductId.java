package com.nbusto.trial.domain.model.vo;

public record ProductId(Integer value) {}
