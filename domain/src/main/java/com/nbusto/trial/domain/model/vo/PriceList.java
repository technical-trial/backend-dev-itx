package com.nbusto.trial.domain.model.vo;

public record PriceList(Integer value) {}
