package com.nbusto.trial.domain.model.vo;

public record Priority(Integer value) {}
