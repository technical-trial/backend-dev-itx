# backend-dev-itx



## Getting started

This is a test project, works with an H2 in-memory database, that is configured
and populated with liquibase.

```mermaid
sequenceDiagram
    actor User
    participant RestService
    participant Database
    
    User ->> RestService: Asks for data
    RestService ->> Database: Query data
    Database ->> RestService: Returns
    RestService ->> User: Retrieves data
```