package com.nbusto.trial.infra.adapter;

import static org.assertj.core.api.BDDAssertions.and;
import static org.instancio.Instancio.create;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

import com.nbusto.trial.domain.model.Price;
import com.nbusto.trial.infra.adapter.PriceForDateAdapter;
import com.nbusto.trial.infra.config.JpaConfig;
import com.nbusto.trial.infra.database.repository.PriceRepository;
import com.nbusto.trial.infra.mapper.PriceMapper;
import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

@SuppressWarnings("AccessStaticViaInstance")
@ContextConfiguration(classes = {PriceMapper.class, PriceForDateAdapter.class, JpaConfig.class})
@DataJpaTest
@ExtendWith(MockitoExtension.class)
class PriceForDateAdapterTest {

  @MockBean private PriceRepository repository;
  @Autowired private PriceForDateAdapter sut;

  @Test
  void when_look_for_not_found_price_then_return_empty() {
    // Given
    Integer brandId = create(Integer.class);
    Integer productId = create(Integer.class);
    LocalDateTime time = create(LocalDateTime.class);

    given(repository.findByBrandProductAndDate(brandId, productId, time))
        .willReturn(Optional.empty());

    // When
    Optional<Price> returnedPrice = sut.getPriceFor(brandId, productId, time);

    // Then
    then(repository).should(times(1)).findByBrandProductAndDate(any(), any(), any());

    and.then(returnedPrice).isEmpty();
  }

  @Test
  void when_look_for_price_then_return_parsed_price() {
    // Given
    Integer brandId = create(Integer.class);
    Integer productId = create(Integer.class);
    LocalDateTime time = create(LocalDateTime.class);

    com.nbusto.trial.infra.database.entities.Price queriedPrice =
        create(com.nbusto.trial.infra.database.entities.Price.class);

    given(repository.findByBrandProductAndDate(brandId, productId, time))
        .willReturn(Optional.of(queriedPrice));

    // When
    Optional<Price> returnedPrice = sut.getPriceFor(brandId, productId, time);

    // Then
    then(repository).should(times(1)).findByBrandProductAndDate(any(), any(), any());

    and.then(returnedPrice)
        .isNotEmpty()
        .get()
        .satisfies(it -> {
            and.then(it.brandId().value()).isEqualTo(queriedPrice.getBrandId());
            and.then(it.startDate().value()).isEqualTo(queriedPrice.getStartDate());
            and.then(it.endDate().value()).isEqualTo(queriedPrice.getEndDate());
            and.then(it.priceList().value()).isEqualTo(queriedPrice.getPriceList());
            and.then(it.productId().value()).isEqualTo(queriedPrice.getProductId());
            and.then(it.priority().value()).isEqualTo(queriedPrice.getPriority());
            and.then(it.price().value()).isEqualTo(queriedPrice.getPrice());
            and.then(it.currency().value()).isEqualTo(queriedPrice.getCurrency());
        });
  }
}
