package com.nbusto.trial.infra.mapper;

import static org.assertj.core.api.BDDAssertions.*;

import com.nbusto.trial.infra.database.entities.Price;
import org.instancio.Instancio;
import org.junit.jupiter.api.Test;

class PriceMapperTest {
  private final PriceMapper sut = new PriceMapper();

  @Test
  void when_entity_is_received_then_data_is_parsed_to_right_fields() {
    // Given
    Price priceEntity = Instancio.create(Price.class);

    // When
    com.nbusto.trial.domain.model.Price domainPrice = sut.toDomain(priceEntity);

    // Then
    then(domainPrice)
        .isNotNull()
        .satisfies(it -> {
            then(it.brandId().value()).isEqualTo(priceEntity.getBrandId());
            then(it.startDate().value()).isEqualTo(priceEntity.getStartDate());
            then(it.endDate().value()).isEqualTo(priceEntity.getEndDate());
            then(it.priceList().value()).isEqualTo(priceEntity.getPriceList());
            then(it.productId().value()).isEqualTo(priceEntity.getProductId());
            then(it.priority().value()).isEqualTo(priceEntity.getPriority());
            then(it.price().value()).isEqualTo(priceEntity.getPrice());
            then(it.currency().value()).isEqualTo(priceEntity.getCurrency());
        });
  }
}
