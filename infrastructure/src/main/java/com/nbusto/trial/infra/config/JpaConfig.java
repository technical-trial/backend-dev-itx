package com.nbusto.trial.infra.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.nbusto.trial.infra.database.repository")
@EntityScan("com.nbusto.trial.infra.database.entities")
public class JpaConfig {}
