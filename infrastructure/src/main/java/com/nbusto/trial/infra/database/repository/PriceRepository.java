package com.nbusto.trial.infra.database.repository;

import com.nbusto.trial.infra.database.entities.Price;
import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface PriceRepository extends Repository<Price, Integer> {

  @Query("select p from Price p "
          + "where p.brandId = :brandId "
          + "and p.productId = :productId "
          + "and :time between p.startDate and p.endDate "
          + "order by p.priority desc "
          + "limit 1")
  Optional<Price> findByBrandProductAndDate(Integer brandId, Integer productId, LocalDateTime time);
}
