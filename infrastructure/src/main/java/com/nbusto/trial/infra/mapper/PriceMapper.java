package com.nbusto.trial.infra.mapper;

import com.nbusto.trial.domain.model.Price;
import com.nbusto.trial.domain.model.vo.BrandId;
import com.nbusto.trial.domain.model.vo.Currency;
import com.nbusto.trial.domain.model.vo.EndDate;
import com.nbusto.trial.domain.model.vo.PriceList;
import com.nbusto.trial.domain.model.vo.Priority;
import com.nbusto.trial.domain.model.vo.ProductId;
import com.nbusto.trial.domain.model.vo.StartDate;
import org.springframework.stereotype.Component;

@Component
public class PriceMapper {

  public Price toDomain(com.nbusto.trial.infra.database.entities.Price price) {
    return new Price(
        new BrandId(price.getBrandId()),
        new StartDate(price.getStartDate()),
        new EndDate(price.getEndDate()),
        new PriceList(price.getPriceList()),
        new ProductId(price.getProductId()),
        new Priority(price.getPriority()),
        new com.nbusto.trial.domain.model.vo.Price(price.getPrice()),
        new Currency(price.getCurrency()));
  }
}
