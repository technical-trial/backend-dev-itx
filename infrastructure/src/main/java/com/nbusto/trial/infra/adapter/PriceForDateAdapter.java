package com.nbusto.trial.infra.adapter;

import com.nbusto.trial.app.port.PriceForDatePort;
import com.nbusto.trial.domain.model.Price;
import com.nbusto.trial.infra.database.repository.PriceRepository;
import com.nbusto.trial.infra.mapper.PriceMapper;
import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.stereotype.Component;

@Component
public class PriceForDateAdapter implements PriceForDatePort {

  private final PriceRepository priceRepository;
  private final PriceMapper priceMapper;

  public PriceForDateAdapter(
          PriceRepository priceRepository,
          PriceMapper priceMapper) {
    this.priceRepository = priceRepository;
    this.priceMapper = priceMapper;
  }

  @Override
  public Optional<Price> getPriceFor(Integer brandId, Integer productId, LocalDateTime time) {
    return priceRepository
        .findByBrandProductAndDate(brandId, productId, time)
        .map(priceMapper::toDomain);
  }
}
