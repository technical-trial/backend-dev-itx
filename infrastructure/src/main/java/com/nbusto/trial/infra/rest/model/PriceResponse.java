package com.nbusto.trial.infra.rest.model;

import com.nbusto.trial.domain.model.Price;
import java.time.LocalDateTime;

public record PriceResponse(
    Integer productId, Integer brandId, Integer priceId, Dates applicationDates) {

  public static PriceResponse fromDomain(Price dto) {
    return new PriceResponse(
        dto.productId().value(),
        dto.brandId().value(),
        dto.priceList().value(),
        new Dates(dto.startDate().value(), dto.endDate().value()));
  }

  public record Dates(LocalDateTime start, LocalDateTime end) {}
}
