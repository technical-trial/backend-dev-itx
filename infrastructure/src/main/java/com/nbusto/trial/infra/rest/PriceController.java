package com.nbusto.trial.infra.rest;

import com.nbusto.trial.infra.rest.model.PriceResponse;
import com.nbusto.trial.app.port.PriceForDatePort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
public class PriceController {

  private final PriceForDatePort priceForDatePort;

  public PriceController(final PriceForDatePort priceForDatePort) {
    this.priceForDatePort = priceForDatePort;
  }

  @GetMapping("/v1/prices/{brand_id}/{product_id}")
  public ResponseEntity<PriceResponse> getPriceForDate(
      @PathVariable("brand_id") Integer brandId,
      @PathVariable("product_id") Integer productId,
      @RequestParam("application_date")
      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
      LocalDateTime dateTime) {

    return priceForDatePort
        .getPriceFor(brandId, productId, dateTime)
        .map(PriceResponse::fromDomain)
        .map(ResponseEntity::ok)
        .orElse(ResponseEntity.notFound().build());
  }
}
